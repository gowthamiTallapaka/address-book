Address Book
Following features are developed as part of this Code Test.

Address book should hold first name, last name  and phone numbers of contact entries

All the list of contacts retrived are in sorted order.

Soring is done based on the hashcode comparision of firstname.
As the hashcode of String is being compared, need to be careful with case senitive.  

To derive unique contacts from all address books again combination hashcode of firstname, lastname and phone number were compared.
For any contact to be unique, combination of firstname, lastname and phonenumber should be unique.


1.Users should be able to add a new contact entry to an existing address book
2.Users should be able to add a new address book with new contact
3.Users should be able to retrieve all contacts from requested address book
4.Users should be abe to retrieve all contacts across all address books
5.Users should be able to retrieve a unique set of all contacts across multiple address books.


Assumptions
There are already two existing address books initialised in memory
REST endpoints for maintaining address books are not required (e.g AddressBook CRUD operations)
Design & implement RESTful API services to meet the above requirements, a working user interface is not required. 
Data should only be persisted in memory. 

Technoloies/Frameworks used- 
Java8
Spring boot
RESTFUL API
Junit (Integration Testing, validation testing, service testing)
SWAGGER - For API documentation


How to use and start
Build
mvn install

Test
mvn test

Start
mvn springboot:run

server will run on 8080 port, port number can be configured in resoures/application.properties

SWAGGER URL- http://localhost:8080/swagger-ui.html#/


REST ENDPOINTS
Get contacts for a given address book
GET http://localhost:8080/api/v1/address-book/AddressBook2

Add Contact
POST http://localhost:8080/api/v1/address-book/AddressBook3

Payload:
{
  "firstName": "Test",
  "lastName": "Test1",
  "phoneNumbers": "988998897"
  
}

Get Unique Contacts
GET http://localhost:8080/api/v1/contacts?unique=true

Get All Contacts
GET http://localhost:8080/api/v1/contacts?unique=false