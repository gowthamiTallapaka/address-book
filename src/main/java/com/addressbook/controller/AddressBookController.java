/**
 * 
 */
package com.addressbook.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.addressbook.model.AddressContact;
import com.addressbook.service.AddressBookService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Gowthami
 * This is the main Rest controller to create and retrieve
 * contacts from address book
 */
@Api(value="Address Book Management System")
@RestController
public class AddressBookController {

	@Autowired
	private AddressBookService addressBookService;

	@PostMapping("/api/v1/address-book/{addressBookId}")
	@ApiOperation(value = "To add new address book contact or to add any new contact in existing address book", response = AddressContact.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully added contact in address book"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 500, message = "Internal Server Error")
	})
	public AddressContact addContactToAddressBook(
			@ApiParam(value = "Address book name", required = true) @PathVariable String addressBookId, 
			@ApiParam(value = "contact details", required = true) @Valid @RequestBody AddressContact newContact) {

		return addressBookService.addContact(addressBookId, newContact);
	}

	@ApiOperation(value = "To retrive all contacts of requested address book ", response = List.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully retrieved contacts of requested address book"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The requested Address book contacts you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Internal Server Error")
	})
	@GetMapping("/api/v1/address-book/{addressBookId}")
	public List<AddressContact> retrieveContactsFromAddressBook(@ApiParam(value = "Address book name", required = true) @PathVariable String addressBookId) {
		return addressBookService.retrieveContacts(addressBookId);
	}

	@ApiOperation(value = "To add new address book contact or to add any new contact in existing address book", response = AddressContact.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully retrieved unique/ all contacts of all address book"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 500, message = "Internal Server Error")
	})
	@GetMapping("/api/v1/contacts")
	public List<AddressContact> retrieveUniqueContactsFromAllAddressBooks( @ApiParam(value = "For unique values this need to be set as true else it will return all contacts including duplicates")
	@RequestParam(value = "unique", defaultValue = "false") Boolean unique) {
		return addressBookService.retrieveAllUniqueContacts(unique);
	}
}
