/**
 * 
 */
package com.addressbook.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.addressbook.exception.Exception;
import com.addressbook.model.AddressContact;


/**
 * @author Gowthami
 *
 *This is the service class with all the necessary business logics
 *to add and retrieve address book contacts
 */
@Service
public class AddressBookService {

	private static final String ADDRESS_BOOK_1 = "AddressBook1";
	private static final String ADDRESS_BOOK_2 = "AddressBook2";

	//TODO: Need to be replaced with appropriate persistence
	private final Map<String, List<AddressContact>> addressBooks = new HashMap<>();

	public AddressBookService() {
		addressBooks.put(ADDRESS_BOOK_1, new ArrayList<>());
		addressBooks.put(ADDRESS_BOOK_2, new ArrayList<>());

		this.addContact(ADDRESS_BOOK_1, new AddressContact("Mary", "Anderson", "0404234567"));
		this.addContact(ADDRESS_BOOK_1, new AddressContact("Bob", "Allan", "0404123456"));
		this.addContact(ADDRESS_BOOK_1, new AddressContact("Jane", "Mill", "0404345678"));

		this.addContact(ADDRESS_BOOK_2, new AddressContact("John", "Bailey","0406234567"));
		this.addContact(ADDRESS_BOOK_2, new AddressContact("Jane", "Mill", "0404345678"));
		this.addContact(ADDRESS_BOOK_2, new AddressContact("Mary", "Anderson", "0404234567"));
	}

	/**
	 * This method is used to add contacts to any existing 
	 * or any new address book.
	 * @param addressBookId - This is name of the address book
	 * @param contact  This is the Contact request sent from API to persist
	 * @return AddressContact This returns the added contact back 
	 * along with ID and address book details.
	 */
	public AddressContact addContact(String addressBookId, AddressContact contact) {
		contact.setId(UUID.randomUUID().toString());
		contact.setBookName(addressBookId);
		if (!addressBooks.containsKey(addressBookId)) {
			addressBooks.put(addressBookId, new ArrayList<>());
		}
		addressBooks.get(addressBookId).add(contact);
		return contact;
	}

	/**
	 * This method is used to retrieve contacts from an  
	 * existing address book.
	 * @param addressBookId - This is name of the address book	  
	 * @return List<AddressContact>- This returns the list
	 * of contacts available in the requested address book. 
	 */
	public List<AddressContact> retrieveContacts(String addressBookId) {
		if (!addressBooks.containsKey(addressBookId)) {
			throw new Exception("AddressBook with the given Id does not exist");
		}
		List<AddressContact> contacts = addressBooks.get(addressBookId);
		contactSorting(contacts);
		return contacts;
	}

	/**
	 * This method is used to retrieve all/unique contacts from an  
	 * existing address book.
	 * Considering that the contact is unique only if it's name and number matches
	 * @param unique - if unique is true then unique contacts 
	 * from all address books will be retrieved else all contacts
	 * from all address books would be retrieved  
	 * @return List<AddressContact>- This returns the full list/unique list
	 * of contacts available in the requested address book as per request. 
	 */
	public List<AddressContact> retrieveAllUniqueContacts(boolean unique) {
		List<AddressContact> contacts = new ArrayList<>();
		addressBooks.values().forEach(contacts::addAll);
		if (unique) {
			contacts= contacts.stream().collect(Collectors.groupingBy(Function.identity(),
					Collectors.counting()))
					.entrySet().stream()
					.filter(e -> e.getValue() <= 1L)
					.map(e -> e.getKey())
					.collect(Collectors.toList());
			contactSorting(contacts);
			return contacts;
		}
		contactSorting(contacts);
		return contacts;
	}

	private void contactSorting(List<AddressContact> contacts) {
		contacts.sort((AddressContact ac1, AddressContact ac2)->ac1.getFirstName().compareTo(ac2.getFirstName())); 
	}

}
