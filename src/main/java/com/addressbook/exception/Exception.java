/**
 * 
 */
package com.addressbook.exception;

/**
 * @author Gowthami
 *
 */
public class Exception extends RuntimeException {
	public Exception (String s) {
		super(s);
	}
}
