/**
 * 
 */
package com.addressbook.model;

import java.util.Objects;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Gowthami
 *
 *Swagger annotations are implemented to define mandatory fields while creating contact
 *
 */
@ApiModel(value = "AddressContact", description = "Address contact values")
public class AddressContact {

	@ApiModelProperty(value = "id", dataType = "java.lang.String")
	private String id;

	@NotNull(message = "Please provide first Name")
	@NotEmpty(message = "Please provide first Name")
	@Pattern(regexp = "^[a-zA-Z][a-zA-Z0-9-]*$", message = "first name must be an alphanumeric and start with character")
	@ApiModelProperty(value = "firstName", dataType = "java.lang.String", required = true)
	private String firstName;

	@NotNull(message = "Please provide last Name")
	@NotEmpty(message = "Please provide last Name")
	@Pattern(regexp = "^[a-zA-Z][a-zA-Z0-9-]*$", message = "last name must be an alphanumeric and start with character")
	@ApiModelProperty(value = "lastName", dataType = "java.lang.String", required = true)
	private String lastName;

	//TODO: more validations using custom validator
	@NotNull(message = "Please provide phone numbers")
	@NotEmpty(message = "Please provide phone numbers")
	@ApiModelProperty(value = "phoneNumbers", dataType = "java.lang.String", required = true)
	String phoneNumbers;

	@ApiModelProperty(value = "bookName", dataType = "java.lang.String")
	String bookName;

	public AddressContact() 
	{

	}
	public AddressContact(String firstName, String lastName,  String phoneNumbers) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumbers = phoneNumbers;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(String phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		AddressContact addressContact = (AddressContact) o;
		return Objects.equals(firstName, addressContact.firstName) &&
				Objects.equals(lastName, addressContact.lastName) &&
				Objects.equals(phoneNumbers, addressContact.phoneNumbers);

	}

	@Override
	public int hashCode() {
		return Objects.hash(firstName, lastName, phoneNumbers);
	}
}
