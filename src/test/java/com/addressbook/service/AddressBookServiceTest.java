/**
 * 
 */
package com.addressbook.service;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.addressbook.model.AddressContact;

/**
 * @author Gowthami
 *
 * This class represents the test cases at service level
 */
public class AddressBookServiceTest {

	private static final String TEST_ADDRESS_BOOK_1 = "TestAddressBook1";
	private static final String TEST_ADDRESS_BOOK_2 = "TestAddressBook2";

	private AddressBookService addressBookService;

	@Before
	public void init() {
		addressBookService = new AddressBookService();
	}
	/**
	 * This is a test case to validate add contact service
	 * 
	 */
	@Test
	public void testAddContact() {
		AddressContact addedContact = addressBookService.addContact(TEST_ADDRESS_BOOK_1, getNewTestContact());
		AddressContact contact = getNewTestContact();
		Assert.assertTrue(contact.equals(addedContact));
	}
	/**
	 * This is a test case to retrieve all contacts from requested address book
	 * 
	 */
	@Test
	public void testGetContactsFromGivenAddressBook() {
		AddressContact addedContact1 = addressBookService.addContact(TEST_ADDRESS_BOOK_1, getNewTestContact());
		AddressContact addedContact2 = addressBookService.addContact(TEST_ADDRESS_BOOK_1, getNewTestContact());
		AddressContact contact3 = getNewTestContact();
		contact3.setFirstName("Jack");
		AddressContact addedContact3 = addressBookService.addContact(TEST_ADDRESS_BOOK_1, contact3);

		List<AddressContact> retrievedContacts = addressBookService.retrieveContacts(TEST_ADDRESS_BOOK_1);
		List<AddressContact> addedContacts = Arrays.asList(addedContact1, addedContact2, addedContact3);

		Assert.assertTrue(addedContacts.containsAll(retrievedContacts));
		Assert.assertTrue(retrievedContacts.containsAll(addedContacts));
	}

	/**
	 * This is a test case to check exception in case requested address book is not available
	 * 
	 */
	@Test
	public void validateAddContactThrowsExceptionWhenAddressBookDoesNotExist() {
		addressBookService.addContact(TEST_ADDRESS_BOOK_1, getNewTestContact());
		try {
			addressBookService.retrieveContacts(TEST_ADDRESS_BOOK_2);
			Assert.fail("Expected an exception when the given address book does not exist.");
		} catch (Exception e) {
			Assert.assertEquals("AddressBook with the given Id does not exist", e.getMessage());
		}
	}

	/**
	 * This is a test case to retrieve unique contacts from all address books
	 * 
	 */
	@Test
	public void testAllUniqueContacts() {

		List<AddressContact> retrievedContactsBeforeAddingDuplicates = addressBookService.retrieveAllUniqueContacts(true);

		//add contact1
		addressBookService.addContact(TEST_ADDRESS_BOOK_1, getNewTestContact());

		//same as contact1
		addressBookService.addContact(TEST_ADDRESS_BOOK_1, getNewTestContact());

		//contact2
		AddressContact contact3 = getNewTestContact();
		contact3.setFirstName("Jack");

		addressBookService.addContact(TEST_ADDRESS_BOOK_2, contact3);

		//same as Contact1
		addressBookService.addContact(TEST_ADDRESS_BOOK_2, getNewTestContact());

		//At this point we added 3 contacts but the below returned list should contain addition of 4 new records
		List<AddressContact> retrievedContactsAfterAddingDuplicates = addressBookService.retrieveAllUniqueContacts(true);

		Assert.assertTrue(1 == retrievedContactsAfterAddingDuplicates.size() - retrievedContactsBeforeAddingDuplicates.size());
	}
	
	/**
	 * This is a test case to retrieve all contacts from all address books
	 * 
	 */
	@Test
	public void testAllContacts() {

		List<AddressContact> retrievedContactsBeforeAddingDuplicates = addressBookService.retrieveAllUniqueContacts(false);

		//add contact1
		addressBookService.addContact(TEST_ADDRESS_BOOK_1, getNewTestContact());

		//same as contact1
		addressBookService.addContact(TEST_ADDRESS_BOOK_1, getNewTestContact());

		//contact2
		AddressContact contact3 = getNewTestContact();
		contact3.setFirstName("Jack");

		addressBookService.addContact(TEST_ADDRESS_BOOK_2, contact3);

		//same as Contact1
		addressBookService.addContact(TEST_ADDRESS_BOOK_2, getNewTestContact());

		//At this point we added 4 contacts but the below returned list should contain addition of 2 new records
		List<AddressContact> retrievedContactsAfterAddingDuplicates = addressBookService.retrieveAllUniqueContacts(false);

		Assert.assertTrue(4 == retrievedContactsAfterAddingDuplicates.size() - retrievedContactsBeforeAddingDuplicates.size());
	}

	private AddressContact getNewTestContact() {
		return new AddressContact("Shelly", "Cox", "0432543123");
	}
}
