/**
 * 
 */
package com.addressbook.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.addressbook.AddressBookApplication;
import com.addressbook.model.AddressContact;
import com.addressbook.service.AddressBookService;

/**
 * @author Gowthami
 * 
 * I have added test cases for all integration APIs for this coding test. 
 * More test cases can be added to make the code more robust
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AddressBookApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class AddressBookControllerIntegrationTest {

	private static final String CONTACTS_END_POINT = "/api/v1/address-book/address-book1";

	@LocalServerPort
	private int port;
	
	TestRestTemplate restTemplate = new TestRestTemplate();

	/**
	 *This is an integration test case to create an address book
	 * 
	 */
	@Test
	public void testAddContact() {
		AddressContact contact = new AddressContact("TestName", "surname", "0412123345");
		HttpEntity<AddressContact> entity = new HttpEntity<>(contact);
		ResponseEntity<AddressContact> response = restTemplate.postForEntity(createURLWithPort(CONTACTS_END_POINT),
				entity, AddressContact.class);
		Assert.assertTrue(response.getStatusCode().value() == 200);
	}
	/**
	 *This is an integration test case to retrieve all contacts of specific address book
	 * 
	 */
	@Test public void testRetrieveAllContactsByAddessBook() {
		ResponseEntity<String>  response=restTemplate.exchange(createURLWithPort("/api/v1/address-book/{addressBookId}"), HttpMethod.GET,
				new HttpEntity<String>(null, null), String.class,"AddressBook1");
		Assert.assertEquals(response.getStatusCode().value(), 200);}
	/**
	 *This is an integration test case to retrieve all contacts from all address books
	 * 
	 */
	@Test public void testRetrieveAllContacts() {
		ResponseEntity<String>  response=restTemplate.exchange(createURLWithPort("/api/v1/contacts?unique={unique}"), HttpMethod.GET,
				new HttpEntity<String>(null, null), String.class,false);
		Assert.assertEquals(response.getStatusCode().value(), 200);}
	/**
	 *This is an integration test case to retrieve  unique contacts from all address books
	 * 
	 */
	@Test
	public void testRetrieveAllUniqueContacts() {
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/v1/contacts?unique={unique}"), HttpMethod.GET,
				new HttpEntity<String>(null, null), String.class,true);
		Assert.assertEquals(response.getStatusCode().value(), 200);
	}

	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}
}
