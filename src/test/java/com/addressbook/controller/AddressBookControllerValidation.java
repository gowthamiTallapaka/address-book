/**
 * 
 */
package com.addressbook.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.addressbook.AddressBookApplication;
import com.addressbook.model.AddressContact;
import com.addressbook.service.AddressBookService;


/**
 * @author Gowthami
 *
 * I have added validation test cases for contact creation in an address book
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AddressBookApplication.class,
webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AddressBookControllerValidation {
	private static final String CONTACTS_END_POINT = "/api/v1/address-book/address-book1/contacts";
	@LocalServerPort
	private int port;
	

	TestRestTemplate restTemplate = new TestRestTemplate();
	/**
	 * This is a validation test case for invalid first name
	 * 
	 */
	@Test
	public void validateAddContactMustReturn400OnInvalidFirstName() {
		AddressContact contact = new AddressContact("123", "last-name",  "0412 123 346");
		HttpEntity<AddressContact> entity = new HttpEntity<>(contact);

		ResponseEntity<String> response = restTemplate.exchange(
				createURLWithPort(CONTACTS_END_POINT),
				HttpMethod.POST, entity, String.class);

		Assert.assertTrue(response.getStatusCode().value() == 400);
	}
	/**
	 * This is a validation test case for invalid last name
	 * 
	 */
	@Test
	public void validateAddContactMustReturn400OnInvalidLastName() {
		AddressContact contact = new AddressContact("Yarn", "$234$",  "0412 123 346");
		HttpEntity<AddressContact> entity = new HttpEntity<>(contact);

		ResponseEntity<String> response = restTemplate.exchange(
				createURLWithPort(CONTACTS_END_POINT),
				HttpMethod.POST, entity, String.class);

		Assert.assertTrue(response.getStatusCode().value() == 400);
	}

	/**
	 * This is a validation test case for empty phone number
	 * 
	 */
	@Test
	public void validateAddContactMustReturn400OnInvalidPhoneNumbners() {
		AddressContact contact = new AddressContact("Yarn", "ween", "");
		HttpEntity<AddressContact> entity = new HttpEntity<>(contact);

		ResponseEntity<String> response = restTemplate.exchange(
				createURLWithPort(CONTACTS_END_POINT),
				HttpMethod.POST, entity, String.class);

		Assert.assertTrue(response.getStatusCode().value() == 400);
	}

	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}

}
